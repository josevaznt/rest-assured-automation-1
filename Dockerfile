# Imagem base com JDK e Maven
FROM maven:3.8.4-openjdk-11 AS build

# Diretório de trabalho do projeto
WORKDIR /restassured-automation

# Copia os arquivos para o diretório de trabalho
COPY . ./

# Baixa as dependências do Maven
RUN mvn dependency:go-offline -B
