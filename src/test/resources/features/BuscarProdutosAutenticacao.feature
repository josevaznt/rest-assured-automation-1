#language: pt
Funcionalidade: Buscar produtos com autenticação

  @env:dummyjson @path_url:auth_products @BPRODA_001 @buscar_produtos @buscar_produtos_autenticacao @regressao
  Cenário: BPRODA001 - Validar consulta de produtos
    Dado que realizo a consulta dos produtos
    Então o status code deve ser "200"
    E a resposta deve estar em conformidade com o JsonSchema "searchAuthProducts.json"

  @env:dummyjson @path_url:auth_products @BPRODA_002 @buscar_produtos @buscar_produtos_autenticacao @regressao
  Cenário: BPRODA002 - Validar dados preenchidos na consulta de produtos
    Dado que realizo a consulta dos produtos
    Então o status code deve ser "200"
    E o campo "products[0].id" não deve estar vazio
    E o campo "products[0].title" não deve estar vazio
    E o campo "products[0].description" não deve estar vazio
    E o campo "products[0].price" não deve estar vazio
    E o campo "products[0].discountPercentage " não deve estar vazio
    E o campo "products[0].rating" não deve estar vazio
    E o campo "products[0].stock" não deve estar vazio
    E o campo "products[0].brand" não deve estar vazio
    E o campo "products[0].category" não deve estar vazio
    E o campo "products[0].thumbnail" não deve estar vazio
    E o campo "products[0].images" não deve estar vazio

  @env:dummyjson @path_url:auth_products @BPRODA_003 @buscar_produtos @buscar_produtos_autenticacao @regressao
  Cenário: BPRODA003 - Validar consulta de produtos com token inválido
    Dado que realizo a consulta dos produtos com token inválido
    Então o status code deve ser "401"
    E os seguintes campos devem estar preenchidos
      """
      name: JsonWebTokenError
      message: Invalid/Expired Token!
      """

  @env:dummyjson @path_url:auth_products @BPRODA_004 @buscar_produtos @buscar_produtos_autenticacao @regressao
  Cenário: BPRODA004 - Validar consulta de produtos sem autenticação
    Dado que realizo a consulta dos produtos sem autenticação
    Então o status code deve ser "403"
    E os seguintes campos devem estar preenchidos
      """
      message: Authentication Problem
      """
