#language: pt
Funcionalidade: Adicionar produtos

  @env:dummyjson @path_url:products_add @ADDPROD_001 @adicionar_produtos @regressao
  Cenário: ADDPROD001 - Validar adição de produto válido
    Dado que realizo a adição do produto utilizando o payload "AddValidProduct.json"
    Então o status code deve ser "201"
    E a resposta deve estar em conformidade com o JsonSchema "AddproductSchema.json"
    E o campo "id" não deve estar vazio
    E o campo "title" não deve estar vazio
    E o campo "description" não deve estar vazio
    E o campo "price" não deve estar vazio
    E o campo "discountPercentage " não deve estar vazio
    E o campo "rating" não deve estar vazio
    E o campo "stock" não deve estar vazio
    E o campo "brand" não deve estar vazio
    E o campo "category" não deve estar vazio
    E o campo "thumbnail" não deve estar vazio

  
