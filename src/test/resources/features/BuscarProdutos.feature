#language: pt
Funcionalidade: Buscar produtos sem autenticação

  @env:dummyjson @path_url:products @BPROD_001 @buscar_produtos @buscar_produtos_sem_autenticacao @regressao
  Cenário: BPROD001 - Validar consulta de produtos
    Dado que realizo a consulta dos produtos
    Então o status code deve ser "200"
    E a resposta deve estar em conformidade com o JsonSchema "searchAuthProducts.json"

  @env:dummyjson @path_url:products @BPROD_002 @buscar_produtos @buscar_produtos_sem_autenticacao @regressao
  Cenário: BPROD002 - Validar dados preenchidos na consulta de produtos
    Dado que realizo a consulta dos produtos
    Então o status code deve ser "200"
    E o campo "products[0].id" não deve estar vazio
    E o campo "products[0].title" não deve estar vazio
    E o campo "products[0].description" não deve estar vazio
    E o campo "products[0].price" não deve estar vazio
    E o campo "products[0].discountPercentage " não deve estar vazio
    E o campo "products[0].rating" não deve estar vazio
    E o campo "products[0].stock" não deve estar vazio
    E o campo "products[0].brand" não deve estar vazio
    E o campo "products[0].category" não deve estar vazio
    E o campo "products[0].thumbnail" não deve estar vazio
    E o campo "products[0].images" não deve estar vazio
