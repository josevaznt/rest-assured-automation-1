#language: pt
Funcionalidade: Buscar usuário

  @env:dummyjson @path_url:users @BUSER_001 @buscar_usuario @regressao
  Cenário: BUSER001 - Validar consulta de usuários
    Dado que realizo a consulta dos usuarios
    Então o status code deve ser "200"
    E a resposta deve estar em conformidade com o JsonSchema "usersSchema.json"

  @env:dummyjson @path_url:users @BUSER_002 @buscar_usuario @regressao
  Cenário: BUSER002 - Validar dados de acesso preenchidos na consulta de usuários
    Dado que realizo a consulta dos usuarios
    Então o status code deve ser "200"
    E o campo "users[0].username" não deve estar vazio
    E o campo "users[0].password" não deve estar vazio
