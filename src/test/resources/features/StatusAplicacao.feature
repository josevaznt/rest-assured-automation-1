#language: pt
Funcionalidade: Status da Aplicação

  @env:dummyjson @path_url:test @STAP_001 @status @regressao
  Cenário: STAP01 - Validar status da aplicação
    Dado que realizo a consulta do status da aplicação
    Então o status code deve ser "200"
    E os seguintes campos devem estar preenchidos
      """
      status: ok
      method: GET
      """
      
  @env:dummyjson @path_url:test @STAP_002 @status @regressao
  Cenário: STAP02 - Validar estrutura da resposta do status da aplicação
    Dado que realizo a consulta do status da aplicação
    Então o status code deve ser "200"
    E a resposta deve estar em conformidade com o JsonSchema "statusAplicacaoSchema.json"