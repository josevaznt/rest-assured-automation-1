#language: pt
Funcionalidade: Buscar produtos por id

  @env:dummyjson @path_url:products_by_id @BPRODID_001 @buscar_produtos @buscar_produtos_por_id @regressao
  Cenário: BPRODID001 - Validar consulta de produtos por id
    Dado que realizo a consulta do produto de id "1"
    Então o status code deve ser "200"
    E a resposta deve estar em conformidade com o JsonSchema "searchProductById.json"

  @env:dummyjson @path_url:products_by_id @BPRODID_002 @buscar_produtos @buscar_produtos_por_id @regressao
  Cenário: BPRODID002 - Validar dados preenchidos na consulta de produtos por id
    Dado que realizo a consulta do produto de id "2"
    Então o status code deve ser "200"
    E o campo "id" não deve estar vazio
    E o campo "title" não deve estar vazio
    E o campo "description" não deve estar vazio
    E o campo "price" não deve estar vazio
    E o campo "discountPercentage " não deve estar vazio
    E o campo "rating" não deve estar vazio
    E o campo "stock" não deve estar vazio
    E o campo "brand" não deve estar vazio
    E o campo "category" não deve estar vazio
    E o campo "thumbnail" não deve estar vazio
    E o campo "images" não deve estar vazio

  @env:dummyjson @path_url:products_by_id @BPRODID_003 @buscar_produtos @buscar_produtos_por_id @regressao
  Cenário: BPRODID003 - Validar dados preenchidos na consulta de produtos por id com formato inválido
    Dado que realizo a consulta do produto de id "teste"
    Então o status code deve ser "404"
    E os seguintes campos devem estar preenchidos
      """ 
      message: Product with id 'teste' not found
      """
      
  @env:dummyjson @path_url:products_by_id @BPRODID_004 @buscar_produtos @buscar_produtos_por_id @regressao
  Cenário: BPRODID004 - Validar dados preenchidos na consulta de produtos por id inexistente
    Dado que realizo a consulta do produto de id "250"
    Então o status code deve ser "404"
    E os seguintes campos devem estar preenchidos
      """
      message: Product with id '250' not found
      """
