package runner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(glue = { "steps", "core" }, monochrome = true, plugin = { "pretty",
		"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:" }, 
		features = "src/test/resources/features/", stepNotifications = true,
		tags = "@regressao")
public class RunTests {

}
