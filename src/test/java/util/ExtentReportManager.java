package util;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;


public class ExtentReportManager {
	private static ExtentReports extent;
	private static ExtentSparkReporter htmlReporter;
	public  static ExtentTest extentTest;

	public static void setupExtentReport() {
		if (extent == null) {
			extent = new ExtentReports();
			htmlReporter = new ExtentSparkReporter("target/extent-reports/report.html");
			extent.attachReporter(htmlReporter);
		}
	}

	public static void createTest(String testName) {
		extentTest = extent.createTest(testName);
	}

	public static void createTest(String feature, String scenario) {
		extentTest = extent.createTest(feature, scenario);
	}
	
	public static void flushReport() {
		extent.flush();
	}
}