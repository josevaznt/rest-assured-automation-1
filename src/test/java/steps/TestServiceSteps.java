package steps;

import core.RequestManager;
import io.cucumber.java.pt.Dado;
import model.Response;

public class TestServiceSteps {
	@Dado("^que realizo a consulta do status da aplicação$")
	public void performAplicationStatusVerifyRequest() throws Throwable {
		Response.setResponse(RequestManager.get());
	}

}
