package steps;

import java.util.LinkedHashMap;

import core.RequestManager;
import io.cucumber.java.pt.Dado;
import model.Response;

public class SearchProductsSteps {
	 @Dado ("^que realizo a consulta dos produtos$")
	 public void perfomSearchUsers() {
		 Response.setResponse(RequestManager.get());
	 }
	 
	 @Dado ("^que realizo a consulta dos produtos com token inválido$")
	 public void perfomSearchUsersWithInalidToken() {
		 RequestManager.setInvalidToken();
		 Response.setResponse(RequestManager.get());
	 }
	 
	 
	 @Dado ("^que realizo a consulta dos produtos sem autenticação$")
	 public void perfomSearchUsersWithoutAuthentication() {
		 RequestManager.cleanToken();
		 Response.setResponse(RequestManager.get());
	 }
	 
	 @Dado ("^que realizo a consulta do produto de id \"(.*?)\"$")
	 public void perfomSearchUsersWithoutAuthentication(String id) {
		 LinkedHashMap<String,String> params = new LinkedHashMap<String, String>();
		 params.put("{id}", id);
		 RequestManager.setBasePathUrlParam(params);
		 Response.setResponse(RequestManager.get());
	 }
	 
	 

}
