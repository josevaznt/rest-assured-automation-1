package steps;

import core.RequestManager;
import io.cucumber.java.pt.Dado;
import model.Response;

public class AddProductSteps {

	@Dado("^que realizo a adição do produto utilizando o payload \"(.*?)\"")
	public void perfomSearchUsersWithInalidToken(String payloadFileName) throws Exception {
		Response.setResponse(RequestManager.post(payloadFileName));
	}

}
