package steps;

import java.io.IOException;

import org.hamcrest.Matchers;
import org.hamcrest.text.IsBlankString;
import org.junit.Assert;

import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.restassured.module.jsv.JsonSchemaValidator;
import model.Response;
import util.JsonManager;

public class commonsSteps {

	@Entao("^o status code deve ser \"(.*?)\"$")
	public void vaidateStatusCode(String expectedStatusCode) throws IOException {
		Assert.assertEquals(expectedStatusCode, String.valueOf(Response.getResponse().getStatusCode()));
	}

	@E("os seguintes campos devem estar preenchidos")
	public void fieldsShouldBeFilledWithExpectedValue(String fieldsExpectedValue) {
		String fieldsExpectedValueList[] = fieldsExpectedValue.split("\\r?\\n");
		for (String fieldAndValue : fieldsExpectedValueList) {
			String field = fieldAndValue.split("\\s*\\:\\s*")[0];
			String expectedValue = fieldAndValue.split("\\s*\\:\\s*")[1];
			String responseValue = JsonManager.getElementByPath(Response.getResponse().getBody().asString(), field);
			Assert.assertEquals(expectedValue, responseValue);
		}
	}
	
	@E("^a resposta deve estar em conformidade com o JsonSchema \"(.*?)\"$")
	   public void theResponseShouldMatchTheJsonSchema(String schemaFilename) {
	       Response.getResponse().then().assertThat().body(Matchers.is(JsonSchemaValidator.matchesJsonSchema(this.getClass().getResourceAsStream("/schemas/"+schemaFilename))));
	  }
	
	@E("^o campo \"([^\"]*)\" não deve estar vazio$")
	public void verifyFieldNotEmpty(String fieldPath) {
		Response.getResponse().then().assertThat().body(fieldPath, Matchers.not(IsBlankString.blankOrNullString()));
	}
}
