# Validação de API Rest com RestAssured

##### Plano de Testes - API REST de Gerenciamento de Usuários e Produtos

## Introdução

Este plano de testes tem como objetivo verificar a funcionalidade e qualidade da API REST de cadastro e busca de produtos e consulta de usuários . Serão realizados testes de diferentes cenários para garantir a correta funcionalidade da API e de seus serviços.   

## Estrátégia de Testes

Para mapeamento dos casos de testes utilizar a técnica de partição de equivalência e a heurística VADER (Verbos, Autenticação, Dados, Erros e Responsividade) para encontrar os possíveis cenários para realização dos testes.

Após realização dos testes manuais e mapeamento dos fluxos será realizada a automação utilizando Java, RestAssured e Maven com geração de relatórios de resultado dos testes implementando ExtentReport.

Além disso, também serão relatados bugs encontrados durante os testes, bem como possíveis melhorias a serem consideradas.

## Cenários de testes

**Fucnionalidade**: Adicionar Produtos

| Cenário                          | Pré-condição    | Ações                                                                                              | Resultado Esperado                                                          | Resultado do teste                                                                       | Status |
| -------------------------------- | --------------- | -------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------- |:------:|
| Validar adição de produto válido | API em execução | Enviar uma requisição post para /products/add com as informações do produto no corpo da requisição | Código de status (201) e retorno das informações inseridas na base de dados | Código de Status (200) e o campo 'discountPercentage' não retornado no corpo da resposta | :x:    |

**Fucnionalidade**: Buscar produtos sem autenticação

| Cenário                                           | Pré-condição                                                      | Ações                                    | Resultado Esperado   | Resultado do teste                                                                           | Status             |
| ------------------------------------------------- | ----------------------------------------------------------------- | ---------------------------------------- | -------------------- | -------------------------------------------------------------------------------------------- |:------------------:|
| Validar consulta de produtos                      | API em execução e JsonSchema para validar a estrutura da resposta | Enviar uma requisição GET para /products | Código de status 200 | Código (200) e lista de produtos retornados com sucesso e estrutura de acordo com JsonSchema | :white_check_mark: |
| Validar dados preenchidos na consulta de produtos | API em execução                                                   | Enviar uma requisição GET para /products | Código de status 200 | Código (200) e campos retornados preenchidos                                                 | :white_check_mark: |

**Fcnionalidade**: Buscar produtos com autenticação

| Cenário                                           | Pré-condição                                             | Ações                                         | Resultado Esperado                                                | Resultado do teste                                                         | Status             |
| ------------------------------------------------- | -------------------------------------------------------- | --------------------------------------------- | ----------------------------------------------------------------- | -------------------------------------------------------------------------- |:------------------:|
| Validar consulta de produtos                      | API em execução e JsonSchema para validação da estrutura | Enviar uma requisição GET para /auth/products | Código de status 200 e retorno da lista de produtos               | Código (200) e estrutura retornada corretamente de acordo com o JsonSchema | :white_check_mark: |
| Validar dados preenchidos na consulta de produtos | API em execução                                          | Enviar uma requisição GET para /auth/products | Código de status 200 e retorno dos dados dos produtos preenchidos | Código (200) e campos preenchidos                                          | :white_check_mark: |
| Validar consulta de produtos com token inválido   | API em execução e Token inválido                         | Enviar uma requisição GET para /auth/products | Código de status 401 e retorno do erro 'Invalid/Expired Token!'   | Código (401) e erro esperado retornado corretamente                        | :white_check_mark: |
| Validar consulta de produtos sem autenticação     | API em execução e Token vazio                            | Enviar uma requisição GET para /auth/products | Código de status 403 e retorno do erro 'Authentication Problem'   | Código (403) e erro esperado retornado corretamente                        | :white_check_mark: |

**Fcnionalidade**: Buscar produtos por id

| Cenário                                                                       | Pré-condição                                            | Ações                                         | Resultado Esperado                                                         | Resultado do teste                                                         | Status             |
| ----------------------------------------------------------------------------- | ------------------------------------------------------- | --------------------------------------------- | -------------------------------------------------------------------------- | -------------------------------------------------------------------------- |:------------------:|
| Validar consulta de produtos por id                                           | API em execução e id de um produto cadastrado           | Enviar uma requisição GET para /products/{id} | Código de status 200 e retorno da lista de produtos                        | Código (200) e estrutura retornada corretamente de acordo com o JsonSchema | :white_check_mark: |
| Validar dados preenchidos na consulta de produtos por id                      | API em execução e id de um produto cadastrado           | Enviar uma requisição GET para /products/{id} | Código de status 200 e retorno dos dados dos produtos preenchidos          | Código (200) e campos preenchidos                                          | :white_check_mark: |
| Validar dados preenchidos na consulta de produtos por id com formato inválido | API em execução e id de um produto com formato inválido | Enviar uma requisição GET para /products/{id} | Código de status 404 e retorno do erro 'Product with id '{id}' not found!' | Código (404) e erro esperado retornado corretamente                        | :white_check_mark: |
| Validar dados preenchidos na consulta de produtos por id inexistente          | API em execução e id de um produto não cadastrado       | Enviar uma requisição GET para /products/{id} | Código de status 404 e retorno do erro 'Product with id '{id}' not found!' | Código (404) e erro esperado retornado corretamente                        | :white_check_mark: |

**Fcnionalidade**: Buscar usuário

| Cenário                                                     | Pré-condição                                                      | Ações                                 | Resultado Esperado                                  | Resultado do teste                                                                                                            | Status             |
| ----------------------------------------------------------- | ----------------------------------------------------------------- | ------------------------------------- | --------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------- |:------------------:|
| Validar consulta de usuários                                | API em execução e JsonSchema para validar a estrutura da resposta | Enviar uma requisição GET para /users | Código de status 200 e retorno da lista de usuários | Código de status (200) porém no 11° usuário da lista não foi encontrado o campo 'City' no caminho '/users/11/company/address' | :x:                |
| Validar dados de acesso preenchidos na consulta de usuários | API em execução                                                   | Enviar uma requisição GET para /users | Código de status 200                                | Código(200) e os campos de username e password retornados preenchidos                                                         | :white_check_mark: |

**Fcnionalidade**: Consultar status da aplicação

| Cenário                                              | Pré-condição                                                      | Ações                                | Resultado Esperado                                    | Resultado do teste                                                                          | Status             |
| ---------------------------------------------------- | ----------------------------------------------------------------- | ------------------------------------ | ----------------------------------------------------- | ------------------------------------------------------------------------------------------- |:------------------:|
| Validar status da aplicação                          | API em execução                                                   | Enviar uma requisição GET para /test | Código de status 200 e retorno do status da aplicação | Código de status (200) e o campo 'status' não retornado no corpo da resposta com valor 'ok' | :white_check_mark: |
| Validar estrutura da resposta do status da aplicação | API em execução e JsonSchema para validar a estrutura da resposta | Enviar uma requisição GET para /test | Código de status 200 e retorno do status da aplicação | Código de status (200), campos e estrutura do corpo da resposta validado por JsonSchema     | :white_check_mark: |

## Bugs e Melhorias

### Bugs Encontrados

1. Ao cadastrar produtos pelo serviço /products/add o status retorno é 200 em vez de 201 e o campo 'discountPercentage' não é exibido na resposta para confirmação dos dados enviados.

2. Ao cadastrar produtos com tipo de dados incorretos por exemplo campo 'price' que deveria aceitar apenas números também está retornando 200 sem erros quando preenchido com valor de texto

3. Ao buscar a lista de usuários pelo serviço /users o 11° usuário não possui o campo 'city' no caminho '/users/11/company/address'.

### Melhorias Propostas

- Melhoria: Adicionar validação de campos obrigatórios no payload da requisição POST de cadastro de produtos para garantir que nenhum campo obrigatório seja omitido no cadastro de produtos.

- Melhoria: Adicionar validação de autorização para cadastros de produtos para garantir que apenas os perfis autorizados podem inserir informações na base.

- Melhoria: Adicionar serviços para cadastro e exclusão de usuarios (POST E DELETE)  como também exclusão de produtos (DELETE) para garantir que maior controle na gestão destas informações e da base de dados.

## Configuração de ambiente e execução dos testes automatizados

#### Requisitos

- Java: 1.8

- Maven

- Docker *(apenas para execução em container dockerizado)*

- Relizar clone do projeto de automação

- Executar os comandos na raíz do projeto para iníciar os testes

#### Execuçao dos testes na máquina host

    mvn test -Dtest=RunTestsCLI -D"cucumber.filter.tags=@regressao"

#### Execução dos testes em container docker

```
docker-compose up -d
```

## Pipeline Gitlab

- Os scripts de configuração da pipeline estão no arquivo **gitlab-ci.yaml.

- A pipeline utilizará um ambiente dockerizado construído a partir do Dockerfile preparado para executar os cenários.

- Foram implementados os estágios **setup**, **install**, **build** e **test**.
  
  - **setup**: realiza construção da imagem docker e configurações de ambiente.
  
  - **install**: realiza a instação das dependências necessárias para execução dos testes
  
  - **build**: realiza a construção do projeto em arquivo jar que pode ser utilizados em futuras implementações na pipeline.
  
  - **test**: realiza as execuções dos cenários de teste de acordo com a tag informada na variável CUCUMBER_TAGS e gera o relatório html com os resultados das validações.

- Para controle dos cenários executados na pipeline está configurada a váriavel **CUCUMBER_TAGS** e podemos utiliza-la para informar qual tag será executada, por padrão o valor é @regressao (neste caso executarà todos os cenários da suite de testes regressivos).

## Execução dos testes na pipeline do gitlab

- Os testes podem ser iniciados por gatilho manual ou por commit.
  
  - **Manual**: Acesse o caminho no menu lateral do gitlab na página do projeto ***Build > Pipelines*** e clique no botão ***Run Pipeline*** para configuração das váriaveis e executar os testes.

- **Commit:** ao realizar um novo commit para a branch os testes serão executada com a variável 'CUCUMBER_TAGS=@regressao' e executará todos os cenários regressivos configurados com a tag informada.

## Visualização de resultados dos testes

#### Relatório html

- Para geração de relatórios foi implementado  a bliblioteca do ExtentReport para geração de arquivo html com o resultados dos testes.

- O arquivo é sobrescrito a cada execução dos testes.

#### local do arquivo

- O arquivo html de relátório se encontra do caminho **'/target/extent-reports/extent-spark-report.html'**.
  
  
- Após a execução dos cenários na pipeline do gitlab o relatório estará disponível para visualização no caminho: "https://gitlab.com/josevaznt/automacao-api-rest-desafio-sicred/-/jobs/{job_number}/artifacts/browse/target/extent-reports/"
  - **{job_number}**: número do job que será criado para execução dos scripts.


